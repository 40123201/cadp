#coding: utf-8
'''
當 CherryPy 程式尚未牽涉 'OPENSHIFT_DATA_DIR' 資料存取時, 近端與遠端程式完全相同.
但若牽涉必須透過網際介面進行 persistent 資料存取時, 則必須區分近端 data 目錄與遠端目錄.
本程式執行需要 brython.py 與 menu.py
'''
import cherrypy
import os
# 將同目錄下的 brython.py 導入
import sys
# 確定程式檔案所在目錄
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
# 將目前檔案所在目錄納入 sys 模組搜尋目錄中
sys.path.append(_curdir)
import brython
# 將同目錄下的 menu.py 導入
import menu

# 以下為產生 html 標註的特定函式
def htmlTitle(title):
    return '''
<!DOCTYPE html>
<html>
<head>
<! charset meta 設定應該在 title 標註之前, 以免因產生編碼錯誤的 title -->
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>'''+title+"</title>"

def htmlCSS(css):
    return '''
<link rel="stylesheet" type="text/css" href="'''+css+'''">
</head>
'''

class Brython(object):

    @cherrypy.expose
    def about(self):
        return htmlTitle("有關本網站")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
本網站為電腦輔助設計實習課程分組網站<br />
採用 CherryPy 與 Python 建置<br />
</body>
</html>
'''

    @cherrypy.expose
    def index(self):
        return htmlTitle("Brython and Pulldown menu")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
'''+brython.BrythonConsole()+ \
'''
</body>
</html>
'''

    @cherrypy.expose
    def Solvespace1(self):
        return htmlTitle("Solvespace drawing")+'''
        </head>Solvespace零件3繪製<br />
        <body>
        <a href=""></a><br />
        1<br /><br />
        <img src="/static/images/solvespace-3-1.jpg"></img><br /><br />
        2<br /><br />
        <img src="/static/images/solvespace-3-2.jpg"></img><br /><br />
        3<br /><br />
        <img src="/static/images/solvespace-3-3.jpg"></img><br /><br />
        4<br /><br />
        <img src="/static/images/solvespace-3-4.jpg"></img><br /><br />
        
        </body>
        </html>
        '''
    @cherrypy.expose
    def Solvespace2(self):
        return htmlTitle("Solvespace combine")+'''
        </head>123
        <body>
        <a href=""></a><br />
        789<br />
        <img src="/static/images/"></img><br />
        456<br />
        <img src="/static/images/"></img><br />
        
        </body>
        </html>
        '''

    @cherrypy.expose
    def creoParts(self):
        return htmlTitle("Creo 零件檔案")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
這裡展示 Creo 零件檔案
</body>
</html>
'''

    @cherrypy.expose
    def introMember1(self):
        return htmlTitle("組員 1 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 江玟菱<br />
組員學號: 40123201<br />
組員相片:<img src="/static/images/01.jpg"></img><br />
組員專長:吃、睡<br />
組員介紹:美麗大方可愛氣質苗條溫柔體貼<br />

</body>
</html>
'''

    @cherrypy.expose
    def introMember2(self):
        return htmlTitle("組員 2 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 吳逸鈞<br />
組員學號: 40123202<br />
組員相片:<img src="/static/images/02.jpg"></img><br />
組員專長:吃! 吃! 吃!<br />
組員介紹:擁有雅典娜的智慧趙薇的美貌並且善良大方活潑<br />

</body>
</html>
'''

    @cherrypy.expose
    def introMember3(self):
        return htmlTitle("組員 3 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 林重佑<br />
組員學號: 40123223<br />
組員相片:<img src="/static/images/23.jpg"></img><br />
組員專長:!!!<br />
組員介紹:聰明帥氣<br />

</body>
</html>
'''
    @cherrypy.expose
    def introMember4(self):
        return htmlTitle("組員 4 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 陳琨揚<br />
組員學號: 40123237<br />
組員相片:<img src="/static/images/23.jpg"></img><br />
組員專長:運動<br />
組員介紹:聰明帥氣<br />

</body>
</html>
'''
    @cherrypy.expose
    def introMember5(self):
        return htmlTitle("組員 5 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 劉哲宇<br />
組員學號: 40123246<br />
組員相片:<img src="/static/images/23.jpg"></img><br />
組員專長:!!!<br />
組員介紹:聰明帥氣<br />

</body>
</html>
'''
    @cherrypy.expose
    def introMember6(self):
        return htmlTitle("組員 6 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 鄭志善<br />
組員學號: 40123251<br />
組員相片:<img src="/static/images/23.jpg"></img><br />
組員專長:!!!<br />
組員介紹:聰明帥氣<br />

</body>
</html>
'''

# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/Brython1.2-20131109-201900':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/Brython1.2-20131109-201900"},
        '/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"}
    }

# 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 雲端執行啟動
    application = cherrypy.Application(Brython(), config = application_conf)
else:
    # 近端執行啟動
    cherrypy.quickstart(Brython(), config = application_conf)
