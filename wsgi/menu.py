#coding: utf-8

# 第一層標題
m1 = "<a href='/about'>有關本站</a>"
m11 = "<a href='/'>Brython程式區</a>"
m111 = "<a href=''>四連桿機構</a>"
m112 = "<a href=''>零件體積表列</a>"
# 第一層標題
m2 = "<a href=''>Solvespace</a>"
m21 = "<a href='/Solvespace1'>Solvespace零件繪製</a>"
m22 = "<a href='/Solvespace2'>Solvespace零件組立</a>"
# 第一層標題
m3 = "<a href=''>Creo</a>"
m31 = "<a href=''>Creo零件繪製</a>"
m32 = "<a href=''>Creo零件組立</a>"
# 第一層標題
m4 = "<a href=''>V-REP</a>"
# 第一層標題
m5 = "<a href=''>Brython 程式計算</a>"
# 第一層標題
m6 = "<a href=''>Pro/Web.Link</a>"
# 第一層標題
m7 = "<a href=''>組員介紹</a>"
m71 = "<a href='/introMember1'>40123201江玟菱</a>"
m72 = "<a href='/introMember2'>40123202吳逸鈞</a>"
m73 = "<a href='/introMember3'>40123223林重佑</a>"
m74 = "<a href='/introMember4'>40123237陳琨揚</a>"
m75 = "<a href='/introMember5'>40123246劉哲宇</a>"
m76 = "<a href='/introMember6'>40123251鄭志善</a>"

# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 = [ \
        [m1, [m11, m111, m112]],  \
        [m2, m21, m22], \
        [m3,m31,m32], \
        [m4], \
        [m5], \
        [m6], \
        [m7, m71, m72, m73, m74,m75,m76]
        ]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
